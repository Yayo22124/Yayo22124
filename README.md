
<h1 align="center">💫 About Me 💫</h1>

<div align="center">
  <img alt="Night Coding" src="https://raw.githubusercontent.com/AVS1508/AVS1508/master/assets/Night-Coding.gif"/>
</div>

Mi nombre es Eli Haziel Ortiz Ramirez y tengo 18 años, soy un estudiante de T.S.U en **Desarrollo de Software Multiplaforma** de la Universidad Tecnológica de Xicotepec de Juárez. Bienvenido a mi perfil de **GitHub**, aquí podrás conocer sobre mis proyectos personales que uso para aprender y obtener habilidades en herramientas para el desarrollo web.

Mis principales interéses son:

- Desarrollo Web FrontEnd con **frameworks** como Angular o React.
- Desarrollo BackEnd para arquitecturas orientadas a **servicios** (API REST);
- Sistemas basados en **Unix** (Linux).
- Manejo y administración de **Bases de Datos** SQL y NoSQL.
- Control y gestión de versiones para proyectos con **Git**, GitHub y GitLab.

<div align="center">
  <h2>🌐 Redes Sociales 🌐</h2>
  
  [![Facebook](https://img.shields.io/badge/Facebook-%231877F2.svg?logo=Facebook&logoColor=white&style=for-the-badge)](https://facebook.com/elihaziel.ortizramirez) [![Instagram](https://img.shields.io/badge/Instagram-%23E4405F.svg?logo=Instagram&logoColor=white&style=for-the-badge)](https://instagram.com/haziel.ortiz22) [![Stack Overflow](https://img.shields.io/badge/-Stackoverflow-FE7A16?logo=stack-overflow&logoColor=white&style=for-the-badge)](https://stackoverflow.com/users/21898612) [![TikTok](https://img.shields.io/badge/TikTok-%23000000.svg?logo=TikTok&logoColor=white&style=for-the-badge)](https://tiktok.com/@h4k0n3) [![Twitter](https://img.shields.io/badge/Twitter-%231DA1F2.svg?logo=Twitter&logoColor=white&style=for-the-badge)](https://twitter.com/yayo100622)
  
</div>
<div align="center">
  <h2>Contáctame</h2>

  [![Outlook](https://img.shields.io/badge/Outlook-%230078D4.svg?logo=Microsoft-Outlook&logoColor=white&style=for-the-badge)](mailto:ya-yo22124@outlook.com) [![LinkedIn](https://img.shields.io/badge/LinkedIn-%230077B5.svg?logo=linkedin&logoColor=white&style=for-the-badge)](https://linkedin.com/in/haziel-ortiz) [![GitHub](https://img.shields.io/badge/GitHub-%23121011.svg?style=for-the-badge&logo=github&logoColor=gray)](https://github.com/Yayo22124) [![GitLab](https://img.shields.io/badge/GitLab-%23FC6D26.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/Yayo22124) [![Portafolio](https://img.shields.io/badge/Portafolio-%23121011.svg?style=for-the-badge&logo=vercel&logoColor=white)](https://yayo22124.vercel.app) 



<div/>

<h1 align="center">💻 Tech Stack 💻</h1>
<div align="center">

<div style="width: 250px; display: inline-block; text-align: left;">
    <h3>Lenguajes</h3>
    <!-- Front End -->

![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white) ![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)
  </div>
<div style="width: 250px; display: inline-block; text-align: left;">
    <h3>Front End</h3>
    <!-- Front End -->

![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white) ![CSS3](https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white) ![Astro](https://img.shields.io/badge/Astro-%23123456?style=for-the-badge&logo=astro&logoColor=white) ![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB) ![React Router](https://img.shields.io/badge/React_Router-CA4245?style=for-the-badge&logo=react-router&logoColor=white) ![Angular](https://img.shields.io/badge/Angular-%23DD0031.svg?logo=angular&logoColor=white&style=for-the-badge) ![Angular Material](https://img.shields.io/badge/Angular_Material-%23E65100.svg?style=for-the-badge&logo=angular&logoColor=white) ![Angular Animations](https://img.shields.io/badge/Angular_Animations-%23FF5733.svg?style=for-the-badge&logo=angular&logoColor=white) ![Angular HTTPClient](https://img.shields.io/badge/Angular_HTTPClient-%233D7E98.svg?style=for-the-badge&logo=angular&logoColor=white) [![Angular Router](https://img.shields.io/badge/Angular_Router-CA4245?style=for-the-badge&logo=angular-router&logoColor=white)](https://angular.io/guide/router) ![TailwindCSS](https://img.shields.io/badge/tailwindcss-%2338B2AC.svg?style=for-the-badge&logo=tailwind-css&logoColor=white)  ![Bootstrap](https://img.shields.io/badge/bootstrap-%238511FA.svg?style=for-the-badge&logo=bootstrap&logoColor=white)
  </div>
  
  <div style="width: 300px; display: inline-block; text-align: left;">
    <h3>Back End</h3>
    <!-- Back End -->

![Express.js](https://img.shields.io/badge/Express.js-%23000000.svg?logo=express&logoColor=white&style=for-the-badge) ![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white) ![MySQL](https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white) ![MariaDB](https://img.shields.io/badge/MariaDB-003545?style=for-the-badge&logo=mariadb&logoColor=white) [![MongoDB](https://img.shields.io/badge/MongoDB-%2347A248.svg?logo=mongodb&logoColor=white&style=for-the-badge)](https://www.mongodb.com/) [![MongoDB Atlas](https://img.shields.io/badge/MongoDB_Atlas-4DB33D?style=for-the-badge&logo=mongodb&logoColor=white)](https://www.mongodb.com/cloud/atlas) ![Sequelize](https://img.shields.io/badge/Sequelize-%236121A8.svg?style=for-the-badge&logo=sequelize&logoColor=white) 



  </div>
<div/>
<div align="center" class="width:300px;">
  <h3>Herramientas y otros<h3/>

![GIT](https://img.shields.io/badge/Git-fc6d26?style=for-the-badge&logo=git&logoColor=white) ![LINUX](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black) ![Figma](https://img.shields.io/badge/figma-%23F24E1E.svg?style=for-the-badge&logo=figma&logoColor=white) ![CCNA](https://img.shields.io/badge/CCNA-Cisco-1A98E8.svg?style=for-the-badge&logo=cisco&logoColor=white)

<div/>

# 📊 GitLab Stats

<div>
  
  <p href="https://github.com/Yayo22124/Yayo22124/blob/main/README.md" align="center">
    <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=Yayo22124&theme=onedark&hide_border=false&include_all_commits=true&count_private=true&layout=compact" alt="Estats Github"/>
  </p>
  <p href="https://github.com/Yayo22124/Yayo22124/blob/main/README.md" align="center">
    <img align="center" src="https://github-readme-stats.vercel.app/api?username=Yayo22124&theme=onedark&hide_border=false&include_all_commits=true&count_private=true&show_icons=true" />
  </p>
  <p href="https://github.com/Yayo22124/Yayo22124/blob/main/README.md" align="center">
    <img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=Yayo22124&theme=onedark&hide_border=false?username=Yayo22124&theme=onedark&hide_border=false&include_all_commits=true&count_private=true&show_icons=true" />
  </p>
  <p href="https://github.com/Yayo22124/Yayo22124/blob/main/README.md" align="center">
      <img src="http://github-profile-summary-cards.vercel.app/api/cards/profile-details?username=Yayo22124&theme=aura_dark" alt="Estadísticas de GitHub" />
  </p>

</div>

## 🏆 GitHub Trophies

<div align="center" style="display: flex; flex-direction: column; gap: 10px;">

![](https://github-profile-trophy.vercel.app/?username=Yayo22124&theme=alduin&no-frame=true&no-bg=true&margin-w=15&margin-h=15)
  
</div>

## ✍️ Random Dev Quote

<div align="center">

  ![](https://quotes-github-readme.vercel.app/api?type=horizontal&theme=gruvbox)
  
</div>

## 🧑‍🚀 Visit my GitHub projects

<a href="https://github.com/Yayo22124/React-TicTacToe.git">
  <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=Yayo22124&repo=React-TicTacToe" />
</a>
<a href="https://github.com/Yayo22124/AWOS-React.git">
  <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=Yayo22124&repo=AWOS-React" />
</a>
<a href="https://github.com/Yayo22124/SazonMaXico.git">
  <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=Yayo22124&repo=SazonMaXico" />
</a>
</a>
<a href="https://github.com/Yayo22124/POSH-Configure.git">
  <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=Yayo22124&repo=POSH-Configure" />
</a>

<!-- Proudly created with GPRM ( https://gprm.itsvg.in ) -->
